const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require("webpack")

const port = 3003;

const config = {
    mode: 'development',
    entry: [
        path.join(__dirname, 'src', 'js', 'index.js'),
    ],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: ['babel-loader', 'eslint-loader']
            },
            {
                test: /\.s?css$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.(ttf|woff2?|svg)$/,
                use: 'file-loader'
            },
            {
                test: /\.(jpg|png)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]'
                    }
                }
            }
            
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new webpack.HotModuleReplacementPlugin(),
    ],
    devtool: 'eval',
    devServer: {
        compress: true,
        contentBase: path.resolve(__dirname, 'dist'),
        hot: true,
        open: true,
        port: port,
        stats: {
            all: false,
            colors: true,
            errors: true,
            timings: true,
            warnings: true,
        }
    }
}

module.exports = config
