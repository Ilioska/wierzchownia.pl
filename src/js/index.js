import '../scss/style.scss'
import React from 'react'
import {render} from 'react-dom'
import App from './App/App'

const placeholder = document.getElementById('react-root')

render(
    <App />,
    placeholder
)