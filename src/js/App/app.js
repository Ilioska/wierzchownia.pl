import React from 'react'
import { hot } from 'react-hot-loader'
import LandingPage from './components/LandingPage'
import ContentContainer from './components/ContentContainer'
import PictureSection from './components/PictureSection'
import ArticleSection from './components/ArticleSection'
import PhotoContainer from './components/PhotoContainer'
import Title from './components/Title'
import GallerySection from './components/GallerySection'
import OfferBoxSection from './components/OfferBoxSection'
import PriceSection from './components/PriceSection'
import ContactSection from './components/ContactSection'
import TextContent from './components/TextContent'
import {offer} from './components/TextCollection'
import ModalNavigation from './components/ModalNavigation'
import ModalBurger from './components/ModalBurger'
import RegionInfoSection from './components/RegionInfoSection'

class App extends React.Component {
    state = {
        classModal: '',
        visible: '',
        titleColor: '',
    }

    componentDidMount() {
        this.backgroundGallery()
        document.addEventListener('scroll', this.openModalNavigation, true)
        window.addEventListener('resize', this.openModalNavigation, true)
        window.addEventListener('resize', this.backgroundGallery, true)
    }

    componentDidUpdate() {
        console.log('halo', this.state.classModal) //eslint-disable-line
    }

    render() {
        return (
            <div className="app__container">
                <LandingPage />
                <ModalBurger 
                    click={this.toggleModalNavigation}
                />
                <ModalNavigation
                    classModal={this.state.classModal}
                    toggleModal={this.toggleModalNavigation}
                />
                <ContentContainer flex="row" id="about">
                    <PictureSection />
                    <ArticleSection />
                </ContentContainer>
                <div className="effect__container-with-depth">
                    <PhotoContainer 
                        photo="photo-container__first-photo"
                        effect="effect__parallax"
                        visible="true"
                    />
                </div>
                <ContentContainer flex="column" id="region">
                    <Title 
                        title="Ciekawe miejsca w pobliżu"
                        color="container-title__color--text"
                    />
                    <RegionInfoSection />
                </ContentContainer>
                <div id="galery" className="effect__container-with-depth effect__height effect__background-color">
                    <Title 
                        title="Galeria"
                        color={this.state.titleColor}
                    />
                    <GallerySection />
                    <PhotoContainer 
                        photo="photo-container__second-photo" 
                        effect="effect__parallax" 
                        visible={this.state.visible}
                    />
                </div>
                <ContentContainer flex="column" id="offer">
                    <Title 
                        title="Oferta"
                        color="container-title__color--text"
                    />
                    <TextContent 
                        textContent={offer} 
                        class="offer-box-section--alignment"
                    >
                        <OfferBoxSection />
                    </TextContent>
                </ContentContainer>
                <div className="effect__container-with-depth">
                    <div className="effect__middle-bar"></div>
                    <div>
                        <PriceSection id="price-list"/>
                        <PhotoContainer 
                            photo="photo-container__third-photo" 
                            effect="effect__parallax" 
                            visible="true"
                        />
                    </div>
                    
                </div>
                <ContactSection id="contact"/>
                <div className="app__footer">
                    <div className="app__footer--centering">
                        <a href="http://www.daam.pl" target="_blank" rel="noopener noreferrer">daam.pl &copy;</a>
                    </div>
                </div>
            </div>
        )
    }

    openModalNavigation = () => {
        const TopViewport = document.getElementById('landing-page').getBoundingClientRect()
        if (window.innerWidth >= 1200 && TopViewport.top < -100) {
            this.setState({classModal: 'landing-page__modal-navigation--on'})
        } 
        if (window.innerWidth >= 1200 && TopViewport.top >= -100 && this.state.classModal === 'landing-page__modal-navigation--on') {
            this.setState({classModal: 'landing-page__modal-navigation--off',})
        }
    }

    toggleModalNavigation = () => {
        if (window.innerWidth < 1200 && this.state.classModal === '') {
            this.setState({classModal: 'landing-page__modal-navigation--on'})
        } 
        if (window.innerWidth < 1200 && this.state.classModal === 'landing-page__modal-navigation--off') {
            this.setState({classModal: 'landing-page__modal-navigation--on'})
        } 
        if (window.innerWidth < 1200 && this.state.classModal === 'landing-page__modal-navigation--on') {
            this.setState({classModal: 'landing-page__modal-navigation--off'})
        } 
    }

    backgroundGallery = () => {
        if (window.innerWidth >= 800) {
            this.setState({
                visible: true,
                titleColor: 'container-title__color--main-name'
            })
        } else {
            this.setState({
                visible: false,
                titleColor: 'container-title__color--text'
            })
        }
    }
}

export default hot(module)(App)