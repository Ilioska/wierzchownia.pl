export const about = {
    p1: 'Zapraszamy do Leśnej Polany - idealnego miejsca odpoczynku dla szukających ciszy i chwili oddechu na łonie natury.',
    p2: 'Miejsce jest otoczone  należącym do posesji lasem, do najbliższych zabudowań jest ok. 500m. W pobliżu znajduje się mała wieś Wierzchownia, położona nad jeziorem o tej samej nazwie. Źródło jednego z potoków do niego wpadających posiada wodę o bogatych walorach leczniczych.',
    p3: 'Leśna Polana to doskonałe miejsce na wędkowanie, spacery i wycieczki rowerowe. Znajdujemy się na terenie <a href="http://www.parki.kujawsko-pomorskie.pl/glpk" target="_blank" class="article-section--link">Górznieńsko-Lidzbarskiego Parku Krajobrazowego</a> na pograniczu trzech województw: kujawsko-pomorskiego, warmińsko-mazurskiego i mazowieckiego, na obszarze Natura 2000. Okoliczne lasy są niezwykle bogate przyrodniczo, obfitują w grzyby i owoce leśne.',
}

export const offer = {
    p1: 'Dom położony jest na polanie otoczonej lasem należącym do posesji. Miejsce jest zatem osłonięte od wiatrów tworząc korzystny mikroklimat.',
    p2: 'Na parterze domu znajduje się pokój dzienny, w pełni wyposażona kuchnia, łazienka z prysznicem oraz sauna. Piętro stanowi sypialnia dla 4 osób.',
    p3: 'Na zewnątrz jest plac zabaw dla dzieci, ogrodzone palenisko oraz grill.',
    p4: 'Do jeziora mamy ok 300m, do dyspozycji naszych Gości jest prywatny pomost.',
    p5: 'W domu jak i na zewnątrz czeka wiele dodatkowych atrakcji, które uprzyjemnią pobyt w Leśnej Polanie.',
}

export const price = {
    p1: 'W terminie od 22.06 do 1.09',
    p2: '* 1350zł za tydzień pobytu',
    p3: 'Z wyłączeniem terminu wakacyjnego',
    p4: '* 980zł za tydzień pobytu',
    p5: 'Pojedyncze dni i weekendy',
    p6: 'Rozbicie namiotu',
    p7: 'Cena do uzgodnienia',
}

export const contact = {
    p1: 'Beata 603 681 012',
    p2: 'Michał 725 241 698',
    p3: 'lesnapolana@wierzchownia.pl',
    p4: '<a href="https://www.facebook.com/Le%C5%9Bna-Polana-335232457230745/" target="_blank" class="contact-section__contact-box--link-fb">Leśna-Polana</a>'
}