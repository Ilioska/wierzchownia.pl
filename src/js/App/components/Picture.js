import React from 'react'
import PropTypes from 'prop-types'

const Picture = (props) => {
    return (
        <div className={`picture ${props.picture} ${props.pictureClass}`}></div>
    )
}

export default Picture

Picture.propTypes = {
    picture: PropTypes.string,
    pictureClass: PropTypes.string,
    class: PropTypes.string,
}