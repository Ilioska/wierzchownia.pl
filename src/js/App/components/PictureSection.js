import React from 'react'
import PictureBox from './PictureBox'

const PictureSection = () => {
    return (
        <div className="picture-section">
            <PictureBox 
                picture="picture--bicycles"
                class="picture-section__picture-box"
            />
            <PictureBox 
                picture="picture--fishing"
                class="picture-section__picture-box"
            />
            <PictureBox 
                picture="picture--mashroom"
                class="picture-section__picture-box"
            />
        </div>
    )
}

export default PictureSection