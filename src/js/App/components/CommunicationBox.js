import React from 'react'
import ContactBox from './ContactBox'
import MapContainer from './MapContainer'
import {contact} from './TextCollection'

const CommunicationBox = () => {
    return(
        <div className="contact-section__communication-box">
            <div className="contact-section__contact-box-container">
                <ContactBox
                    icon="phone"
                />
                <ContactBox 
                    contact={contact.p1}
                />
                <ContactBox 
                    contact={contact.p2}
                />
                <ContactBox
                    icon="envelope"
                />
                <ContactBox 
                    contact={contact.p3}
                />
                <ContactBox
                    icon="facebook-square"
                    class="contact-section__contact-box--fb"
                />
                <ContactBox 
                    contact={contact.p4}
                />
            </div>
            <MapContainer
                googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUag1LRSOmsLUc1NyScmthT5OPWk1p3q4&v=3.exp&libraries=geometry,drawing,places"
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div className="map-section" />}
                mapElement={<div style={{ height: `100%`, width: '100%' }} />}
            />
        </div>
    )
}

export default CommunicationBox