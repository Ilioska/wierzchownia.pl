import React from 'react'
import Picture from './Picture'
import PropTypes from 'prop-types'

const PictureBox = (props) => {
    return (
        <div className={`${props.class}`}>
            <Picture
                picture={props.picture}
                pictureClass={props.pictureClass}
            />
            <span>{props.signature}</span>
        </div>
    )
}

export default PictureBox

PictureBox.propTypes = {
    class: PropTypes.string,
    picture: PropTypes.string,
    pictureClass: PropTypes.string,
    signature: PropTypes.string,
}