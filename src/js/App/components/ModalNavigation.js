import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import NavLink from './NavLink'

class ModalNavigation extends React.Component {
    render() {
        return ReactDOM.createPortal(   //return this.props.visible ? 
            <div className={`landing-page__modal-navigation ${this.props.classModal}`} onClick={this.props.toggleModal}>
                <NavLink 
                    link="landing-page" //id
                    text="Strona główna"
                />
                <NavLink 
                    link="about"
                    text="O nas"
                />
                <NavLink
                    link="region"
                    text="Okolica"
                />
                <NavLink
                    link="galery"
                    text="Galeria"
                />
                <NavLink
                    link="offer"
                    text="Oferta"
                />
                <NavLink
                    link="price-list"
                    text="Cennik"
                />
                <NavLink
                    link="contact"
                    text="Kontakt"
                />
            </div>,
            document.body
        )
    }
}

export default ModalNavigation

ModalNavigation.propTypes = {
    classModal: PropTypes.string,
    toggleModal: PropTypes.func,
}