import React from 'react'
import PropTypes from 'prop-types'

const Title = (props) => {
    return (
        <span className={`container-title ${props.color}`}>{props.title}</span>
        
    )
}

export default Title

Title.propTypes = {
    title: PropTypes.string,
    color: PropTypes.string,
    background: PropTypes.string,
}