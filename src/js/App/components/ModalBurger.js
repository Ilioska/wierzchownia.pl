import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'

const ModalBurger = (props) => {
    return  ReactDOM.createPortal(
        <div id="burger"className="landing-page__modal-burger" onClick={props.click}>
            <span>Menu</span>
            <i className="jam jam-menu"/>
        </div>,
        document.body
    )
}

export default ModalBurger

ModalBurger.propTypes = {
    click: PropTypes.func,
}