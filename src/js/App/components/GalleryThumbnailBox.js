import React from 'react'
import PropTypes from 'prop-types'

class GalleryThumbnailBox extends React.Component {
    state = {
        src: this.props.src,
    }

    render() {
        return(
            <div className="gallery-section__photo-box" onClick={() => this.props.openModal(this.props.src)}>
                <div className="gallery-section__photo-miniature" style={{backgroundImage: `url(${this.props.src})`}} />
            </div>
        )
    }    
}

export default GalleryThumbnailBox

GalleryThumbnailBox.propTypes = {
    openModal: PropTypes.func,
    src: PropTypes.string,
}