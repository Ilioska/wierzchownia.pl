import React from 'react'
import PropTypes from 'prop-types'

const PriceBox = (props) => {
    return(
        <div className="price-section__price-box">
            <div className="price-section__price-box-info">
                <span>{props.textTop}</span>
            </div>
            <div className="price-section__price-box-price">
                <span>{props.textBottom}</span>
            </div>
        </div>
    )
}

export default PriceBox

PriceBox.propTypes = {
    textBottom: PropTypes.string,
    textTop: PropTypes.string,
}