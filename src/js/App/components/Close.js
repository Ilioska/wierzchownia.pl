import React from 'react'
import PropTypes from 'prop-types'

const Close = (props) => {
    return(
        <div className="modal__close" onClick={props.close}>
            <p className="modal__close--word">Close</p>
            <i className="jam jam-close-rectangle modal__close--sign"></i>       
        </div>
    )
}

export default Close

Close.propTypes = {
    close: PropTypes.func,
}