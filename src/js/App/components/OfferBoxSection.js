import React from 'react'
import PictureBox from './PictureBox'

const OfferBoxSection = () => {
    return(
        <div className="offer-box-section">
            <PictureBox 
                picture="picture--fireplace"
                class="offer-box-section__picture-box"
                pictureClass="picture--offer-size"
                signature="kominek"
            />
            <PictureBox 
                picture="picture--sauna"
                class="offer-box-section__picture-box"
                pictureClass="picture--offer-size"
                signature="sauna"
            /> 
            <PictureBox 
                picture="picture--badminton"
                class="offer-box-section__picture-box"
                pictureClass="picture--offer-size"
                signature="badminton"
            />
            <PictureBox 
                picture="picture--fun-in-lake"
                class="offer-box-section__picture-box"
                pictureClass="picture--offer-size"
                signature="zabawy w jeziorze"
            />
            <PictureBox 
                picture="picture--boule"
                class="offer-box-section__picture-box"
                pictureClass="picture--offer-size"
                signature="boule"
            />
            <PictureBox 
                picture="picture--blocks"
                class="offer-box-section__picture-box"
                pictureClass="picture--offer-size"
                signature="zabawki"
            />
            <PictureBox 
                picture="picture--crayons"
                class="offer-box-section__picture-box"
                pictureClass="picture--offer-size"
                signature="kolorowanki"
            />
            <PictureBox 
                picture="picture--board-games"
                class="offer-box-section__picture-box"
                pictureClass="picture--offer-size"
                signature="gry planszowe"
            />
        </div>
    )
}

export default OfferBoxSection