import React from 'react'
import PropTypes from 'prop-types'

class PhotoContainer extends React.Component {
    render() {
        return this.props.visible ? 
            (        
                <div id={this.props.id} className={`photo-container ${this.props.photo} ${this.props.effect}`}>
                    {this.props.children}
                </div>
            )
            :
            null
    }
}

export default PhotoContainer

PhotoContainer.propTypes = {
    id: PropTypes.string,
    photo: PropTypes.string,
    effect: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    visible: PropTypes.bool,
}