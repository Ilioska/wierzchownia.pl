import React from 'react'
import GalleryThumbnailBox from './GalleryThumbnailBox'

import Modal from './Modal'

const gallery = require.context('../../../images/gallery', false, /\.jpg$/)

class GallerySection extends React.Component {
    state = {
        isModalVisible: false,
        index: null,
        gallery: gallery.keys(),
        addclass: ''
    }

    render() {
        return(
            <div className="gallery-section">
                {this.state.gallery.map((el) => {
                    return <GalleryThumbnailBox
                        key={el}
                        src={el}
                        openModal={this.openModal}
                    />
                })}
                <Modal 
                    visible={this.state.isModalVisible}
                    closeModal={this.closeModal}
                    src={this.state.gallery[this.state.index]}
                    setNextPhoto={this.setNextPhoto}
                    setPrevPhoto={this.setPrevPhoto}
                    addclass={this.state.addclass}
                />
            </div>
        )
    }
    
    openModal = (src) => {
        this.setState({
            isModalVisible: true, 
            src,
            index: this.state.gallery.indexOf(src),
            addclass: 'modal__light-box--open'
        })
    }

    closeModal = () => {
        this.setState({
            addclass: 'modal__light-box--close',
            isModalVisible: false, 
            src: null,
        })
    }

    setNextPhoto = () => {
        this.setState(prevState => {
            return {
                index: prevState.index === this.state.gallery.length - 1 ? 0 : prevState.index + 1,
            }
        }) 
    }

    setPrevPhoto = () => {
        this.setState(prevState => {
            return {
                index: prevState.index === 0 ? this.state.gallery.length - 1 : prevState.index - 1,
            }
        })
    }
}

export default GallerySection