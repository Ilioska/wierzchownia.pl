import React from 'react'
import Title from './Title'
import CommunicationBox from './CommunicationBox'
import PropTypes from 'prop-types'

const ContactSection = (props) => {
    return(
        <div id={props.id} className="contact-section">
            <Title 
                title="Kontakt"
                color="container-title__color--main-name"
            />
            <CommunicationBox />
        </div>
    )
}

export default ContactSection

ContactSection.propTypes = {
    id: PropTypes.string,
}