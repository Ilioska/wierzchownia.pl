import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import LightBox from './Lightbox'
import Close from './Close'

class ModalRegion extends React.Component {
    render() {
        return this.props.visible ? ReactDOM.createPortal(
            <div className="modal modal__region--color jam">
                {<LightBox 
                    src={this.props.src}
                    size='modal__region--size'
                />}
                <Close 
                    close={this.props.closeModal}
                />
            </div>,
            document.body
        ) : null
    }
}

export default ModalRegion

ModalRegion.propTypes = {
    visible: PropTypes.bool,
    closeModal: PropTypes.func,
    src: PropTypes.string,
}