import React from 'react'

const Logo = () => {
    return (
        <span className="landing-page__logo">Leśna Polana</span>
    )
}

export default Logo