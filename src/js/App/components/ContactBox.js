import React from 'react'
import PropTypes from 'prop-types'

const ContactBox = (props) => {
    return(
        <div className="contact-section__contact-box">
            <i className={`jam jam-${props.icon} ${props.class}`}></i>
            <span dangerouslySetInnerHTML={{__html: props.contact}}/>
        </div>
    )
}

export default ContactBox

ContactBox.propTypes = {
    contact: PropTypes.string,
    icon: PropTypes.string,
    class: PropTypes.string,
}