import React from 'react'
import NavLink from './NavLink'

const Navigation = () => {
    return (
        <div className="landing-page__navigation">
            <NavLink 
                link="landing-page" //id
                text="Strona główna"
            />
            <NavLink 
                link="about"
                text="O nas"
            />
            <NavLink
                link="region"
                text="Okolica"
            />
            <NavLink
                link="galery"
                text="Galeria"
            />
            <NavLink
                link="offer"
                text="Oferta"
            />
            <NavLink
                link="price-list"
                text="Cennik"
            />
            <NavLink
                link="contact"
                text="Kontakt"
            />
        </div>
    )
}

export default Navigation