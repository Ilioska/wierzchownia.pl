import React from 'react'
import PropTypes from 'prop-types'

class LightBox extends React.Component {
    state = {}

    render() {
        return(
            <div className={`modal__light-box ${this.props.addclass}`}>
                <img src={`./${this.props.src}`} className={`${this.props.size}`}/>
            </div>
        )
    }
}

export default LightBox

LightBox.propTypes = {
    src: PropTypes.string,
    addclass: PropTypes.string,
    size: PropTypes.string,
}