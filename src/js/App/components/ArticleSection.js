import React from 'react'
import Title from './Title'
import TextContent from './TextContent'
import {about} from './TextCollection'

const ArticleSection = () => {
    return (
        <div className="article-section">
            <Title 
                title="O nas"
            />
            <TextContent 
                textContent={about}
                class="article-section--alignment"
            />
        </div>
    )
}

export default ArticleSection