import React from 'react'
import PriceBox from './PriceBox'
import {price} from './TextCollection'

const PriceList = () => {
    return(
        <div className="price-section__price-list">
            <PriceBox 
                textTop={price.p1}
                textBottom={price.p2}
            />
            <PriceBox 
                textTop={price.p3}
                textBottom={price.p4}
            />
            <PriceBox 
                textTop={price.p5}
                textBottom={price.p7}
            />
            <PriceBox 
                textTop={price.p6}
                textBottom={price.p7}
            />
        </div>
    )
}

export default PriceList