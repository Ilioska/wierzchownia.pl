import React from 'react'
import PropTypes from 'prop-types'

const TextContent = (props) => {
    const keys = Object.keys(props.textContent)
    return (
        <div className={props.class} style={{padding:props.padding}}>
            {keys.map(k => {
                return <p key={k} className="text-content" dangerouslySetInnerHTML={{__html: props.textContent[k]}}/>
            })}
            {props.children}
        </div>
    )
}

export default TextContent

TextContent.propTypes = {
    textContent: PropTypes.object,
    padding: PropTypes.string,
    children: PropTypes.object,
    class: PropTypes.string,
}