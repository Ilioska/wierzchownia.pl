import React from 'react'
import PropTypes from 'prop-types'

const NavLink = (props) => {
    const moveTo = () => {
        const link = document.getElementById(props.link)

        /*if (window.innerWidth > 1200) {
            link.scrollIntoView(props.link !== 'contact') //jeśli true, to ustawia się do górnej krawędzi elementu, jeśli false - do dolnej
        } else {link.scrollIntoView(props.link)} */

        link.scrollIntoView(props.link !== 'contact' || (props.link === 'contact' && link.getBoundingClientRect().height > window.innerHeight))
        
        if (window.innerWidth <= 1200) {
            const navigation = document.querySelector('.landing-page__modal-navigation--on')
            navigation.classList.remove('landing-page__modal-navigation--on')
        }
    }

    return (
        <a onClick={moveTo}><div className="landing-page__nav-link">{props.text}</div></a>
    )
}

export default NavLink

NavLink.propTypes = {
    text: PropTypes.string,
    link: PropTypes.string,
}
