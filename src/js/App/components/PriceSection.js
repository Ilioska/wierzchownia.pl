import React from 'react'
import Title from './Title'
import PriceList from './PriceList'
import PropTypes from 'prop-types'

const PriceSection = (props) => {
    return(
        <div id={props.id} className="price-section">
            <Title 
                title="Cennik"
                color="container-title__color--main-name"
            />
            <PriceList />
            <div className="price-section__price-list-postscript--alignment">
                <div className="price-section__price-list-postscript">
                    <span>* Cena obejmuje nocleg w domu dla 4 osób z możliwością dostawki</span>
                </div>
            </div>
        </div>
    )
}

export default PriceSection

PriceSection.propTypes = {
    id: PropTypes.string,
}
