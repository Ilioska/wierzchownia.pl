import React from 'react'
import PropTypes from 'prop-types'
 
class ContentContainer extends React.Component {
    state = {
        heightAbout: '',
    }

    render() {
        return (
            <div id={this.props.id} className={`content-container content-container__flex--${this.props.flex}`} style={{padding: this.props.padding}}>
                {this.props.children}
            </div>
        )
    }  
//      {heightAbout: document.getElementById('about').offsetHeight}
}

export default ContentContainer

ContentContainer.propTypes = {
    id: PropTypes.string,
    flex: PropTypes.string,
    padding: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
}