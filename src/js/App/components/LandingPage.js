import React from 'react'
import Navigation from './Navigation'
import Logo from './Logo'
import PhotoContainer from './PhotoContainer'

const LandingPage = () => {
    const smoothScroll = () => {
        document.getElementById('about').scrollIntoView({behavior: 'smooth'})
    }

    return (
        <div id="landing-page" className="landing-page">
            <Navigation />
            <Logo />
            <i className="jam jam-chevrons-square-down landing-page__arrow-down" onClick={smoothScroll}/>
            <PhotoContainer photo="photo-container__landing-page-photo" effect="effect__zooming" visible="true"/>
        </div>
    )
}

export default LandingPage