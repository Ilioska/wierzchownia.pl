import React from 'react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps'

const MapContainer = withScriptjs(withGoogleMap(() =>
    <GoogleMap
        defaultZoom={10}
        defaultCenter={{ lat: 53.1580241, lng: 19.6576893 }}
    >
        <Marker position={{ lat: 53.1580241, lng: 19.6576893 }} />
    </GoogleMap>
))

export default MapContainer