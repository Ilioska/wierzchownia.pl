import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import LightBox from './Lightbox'
import Close from './Close'

class Modal extends React.Component {

    componentDidMount() {
        let clientX
        document.addEventListener('touchstart', (e) => {
            clientX = e.touches[0].clientX
        }, false)
        document.addEventListener('touchend', (e) => {
            const deltaX = e.changedTouches[0].clientX - clientX
            if (deltaX < 0) {
                this.props.setNextPhoto()
            } if (deltaX > 0) {
                this.props.setPrevPhoto()
            } else null
        }, false)
    }

    render() {
        return this.props.visible ? ReactDOM.createPortal(
            <div className="modal jam">
                <i className="jam jam-chevron-square-left modal__arrow-left" onClick={this.props.setPrevPhoto}></i>
                {<LightBox 
                    src={this.props.src}
                    addclass={this.props.addclass}
                />}
                <i className="jam jam-chevron-square-right modal__arrow-right" onClick={this.props.setNextPhoto}></i>
                <Close 
                    close={this.props.closeModal}
                />
            </div>,
            document.body
        ) : null
    }
}

export default Modal

Modal.propTypes = {
    addclass: PropTypes.string,
    visible: PropTypes.bool,
    closeModal: PropTypes.func,
    src: PropTypes.string,
    setNextPhoto: PropTypes.func,
    setPrevPhoto: PropTypes.func,
    touchStart: PropTypes.func,
    touchEnd: PropTypes.func,
}