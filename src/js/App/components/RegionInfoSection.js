import React from 'react'
import ModalRegion from './ModalRegion'

class RegionInfoSection extends React.Component {
    state = {
        visible: false,
    }

    render() {
        return(
            <div className='region-info-section' onClick={this.openModalRegion}>
                <ModalRegion 
                    closeModal={this.closeModal}
                    src='wierzchownia.png'
                    visible={this.state.visible}
                />
            </div>
        )
    }

    openModalRegion = () => {
        if (window.innerWidth < 800 && this.state.visible === false) {
            this.setState({visible: true})
        } 
    }

    closeModal = () => {
        this.setState({visible: false})
    }
}

export default RegionInfoSection